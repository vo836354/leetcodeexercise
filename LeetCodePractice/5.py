class Solution:
    def longestPalindrome(self, s: str) -> str:
        if not s:
            return ""
        elif len(s) == 1:
            return s
        elif len(s) == 2:
            if s[0] == s[1]:
                return s
            else:
                return s[0]
        else:
            max_len = 0
            max_str = ""
            for i in range(len(s)):
                for j in range(i+1, len(s)+1):
                    if self.isPalindrome(s[i:j]) and len(s[i:j]) > max_len:
                        max_len = len(s[i:j])
                        max_str = s[i:j]
            return max_str

    def isPalindrome(self, s: str) -> bool:
        if not s:
            return False
        elif len(s) == 1:
            return True
        else:
            for i in range(len(s)//2):
                if s[i] != s[len(s)-i-1]:
                    return False
            return True
